package di.kiss.bonnet_beer.domain.useCases

import di.kiss.bonnet_beer.domain.repositories.PunkRepository
import org.koin.core.KoinComponent

class GetBeersById (private val punkRepository: PunkRepository) : KoinComponent {

    operator fun invoke(id: Int) = punkRepository.getBeersById(id)
}