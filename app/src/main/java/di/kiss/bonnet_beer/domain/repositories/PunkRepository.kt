package di.kiss.bonnet_beer.domain.repositories

import di.kiss.bonnet_beer.domain.entities.Beer
import di.kiss.bonnet_beer.domain.utils.Result

interface PunkRepository {

    fun getBeersById(id: Int)
            : di.kiss.bonnet_beer.domain.utils.Result<List<Beer>>

    fun getBeersList(page: Int, perPage: Int)
            : di.kiss.bonnet_beer.domain.utils.Result<List<Beer>>

    fun getSearchBeer(
        beerName: String,
        page: Int,
        perPage: Int
    )
            : Result<List<Beer>>
}