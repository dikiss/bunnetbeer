package di.kiss.bonnet_beer.domain.useCases

import di.kiss.bonnet_beer.domain.repositories.PunkRepository
import org.koin.core.KoinComponent

class GetBeerList(private val punkRepository: PunkRepository) : KoinComponent {

    operator fun invoke(page: Int, perPage: Int) = punkRepository.getBeersList(page, perPage)
}