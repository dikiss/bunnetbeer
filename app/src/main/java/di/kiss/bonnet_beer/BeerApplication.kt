package di.kiss.bonnet_beer

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import di.kiss.bonnet_beer.di.repositoriesModule
import di.kiss.bonnet_beer.di.sharedPreferences
import di.kiss.bonnet_beer.di.useCasesModule
import di.kiss.bonnet_beer.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BeerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Fresco.initialize(this)

        startKoin {
            androidContext(this@BeerApplication)
            modules(listOf(repositoriesModule, viewModelModule, useCasesModule, sharedPreferences))
        }
    }
}