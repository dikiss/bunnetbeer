package di.kiss.bonnet_beer.di

import android.content.Context
import android.content.SharedPreferences
import di.kiss.bonnet_beer.data.repositories.PunkRepositoryImpl
import di.kiss.bonnet_beer.data.service.PunkService
import di.kiss.bonnet_beer.domain.repositories.PunkRepository
import di.kiss.bonnet_beer.domain.useCases.GetBeerList
import di.kiss.bonnet_beer.domain.useCases.GetBeersById
import di.kiss.bonnet_beer.domain.useCases.GetSearchBeer
import di.kiss.bonnet_beer.ui.utils.SharedPreferencesConfig
import di.kiss.bonnet_beer.ui.viewmodels.PunkViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoriesModule = module {
    single { PunkService() }
    single<PunkRepository> { PunkRepositoryImpl(get()) }
}

val viewModelModule = module {
    single { PunkViewModel(get(), get(), get(), get()) }
}

val useCasesModule = module {
    single { GetBeersById(get()) }
    single { GetBeerList(get()) }
    single { GetSearchBeer(get()) }
}

val sharedPreferences = module {

    single { SharedPreferencesConfig(androidContext()) }

    single {
        getSharedPrefs(androidContext(), "com.example.android.PREFERENCE_FILE")
    }

    single<SharedPreferences.Editor> {
        getSharedPrefs(androidContext(), "com.example.android.PREFERENCE_FILE").edit()
    }
}

fun getSharedPrefs(context: Context, fileName: String): SharedPreferences {
    return context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
}