package di.kiss.bonnet_beer.data.mapper

import di.kiss.bonnet_beer.data.service.response.PunkResponse
import di.kiss.bonnet_beer.domain.entities.Beer


class PunkMapperService : BaseMapperRepository<PunkResponse, Beer> {

    override fun transform(type: PunkResponse): Beer =
        Beer(
            type.id,
            type.name,
            type.description,
            type.tagline,
            type.imageURL,
            type.abv,
            type.ibu
        )

    override fun transformToRepository(type: Beer): PunkResponse =
        PunkResponse(
            type.id,
            type.name,
            type.description,
            type.tagline,
            type.imageURL,
            type.abv,
            type.ibu
        )
}