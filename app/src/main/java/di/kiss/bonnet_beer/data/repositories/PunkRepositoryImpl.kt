package di.kiss.bonnet_beer.data.repositories

import di.kiss.bonnet_beer.data.service.PunkService
import di.kiss.bonnet_beer.domain.entities.Beer
import di.kiss.bonnet_beer.domain.repositories.PunkRepository
import di.kiss.bonnet_beer.domain.utils.Result

class PunkRepositoryImpl(
    private val punkService: PunkService
) : PunkRepository {

    override fun getBeersById(id: Int): Result<List<Beer>> {
        return punkService.getBeersById(id)
    }

    override fun getBeersList(page: Int, perPage: Int): Result<List<Beer>> {
        return punkService.getBeersList(page, perPage)
    }

    override fun getSearchBeer(beerName: String, page: Int, perPage: Int): Result<List<Beer>> {
        return punkService.getSearchBeer(beerName, page, perPage)
    }
}